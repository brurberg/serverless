package prog

import (
  "fmt"
  "log"
  "os"
  //"io"
)

func SettupLoging() *os.File {
  fileName := os.Args[0] + "LogOut"
  logFile, err := os.Stat(fileName)
  if err != nil {
    if !os.IsNotExist(err) {
      log.Fatal(fmt.Sprintf("Could not open file, error: %+v", err))
    } else {
      log.Output(0, "Creating a new file")
    }
  } else {
    log.Output(0, "Appending new line")
  }

  var file *os.File
  if logFile != nil {
    file, err = os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, logFile.Mode())
  } else {
    file, err = os.OpenFile(fileName, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
  }
  //defer file.Close()
  log.SetOutput(file)
  return file
}

/*
func init() {
  SettupLoging()
}
*/
