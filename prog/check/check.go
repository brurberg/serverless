package check

import (
  "os"
  "log"
  //"fmt"
  "net/http"
  "encoding/json"
)

func BoolC(check bool, message string, code int) {
  if check {
    //fmt.Printf(`{"statuscode": %d, "error": "%s"}`, code, message)
    out := make(map[string]interface{})
    out["statuscode"] = code
    out["error"] = message
    json.NewEncoder(os.Stdout).Encode(out)
    log.Fatalf("%s: Error code: %d\n", message, code)
  }
}

func Bool(check bool, message string) {
  BoolC(check, message, http.StatusInternalServerError)
}

func ErrC(err error, code int) {
  if err != nil {
    BoolC(true, err.Error(), code)
  }
}

func Err(err error) {
  ErrC(err, http.StatusInternalServerError)
}
