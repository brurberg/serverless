package helper

import (
  "database/sql"
  "fmt"
  //_ "github.com/lib/pq"
  _ "github.com/go-sql-driver/mysql"
  // "time"
  "github.com/gin-gonic/gin"
  "net/http"
  "log"
  //"encoding/json"
)

type DBConnect struct {
  DB_HOST string
  DB_USER string
  DB_PASSWORD string
  DB_NAME string
}

type DB struct {
  Conn *sql.DB
}


func (dbc DBConnect) ConnectToDB() (*sql.DB, error) {
  dbinfo := ""
  if dbc.DB_PASSWORD == "" {
    dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
      dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
  } else {
    //dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
    dbinfo = fmt.Sprintf("%s:%s@tcp(%s:3306)/%s",
      dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_HOST, dbc.DB_NAME)
  }
  log.Println(dbinfo)

  db, err := sql.Open("mysql", dbinfo)
  return db, err
}
func CheckErr(c *gin.Context, err error) bool {
  return CheckErrCode(c, err, http.StatusInternalServerError)
}

func CheckErrCode(c *gin.Context, err error, errorCode int) bool {
  if (err != nil) {
    log.Println(err)
    if errorCode == 0 {
      errorCode = http.StatusInternalServerError
    }
    c.JSON(errorCode, gin.H{"error": err.Error()})
    return true
  } else {
    return false
  }
}

func CheckRowAff(c *gin.Context, result sql.Result) bool {
  rowAff, err := result.RowsAffected()
  if rowAff < 1 {
    log.Printf("%s\n", err)
    c.JSON(http.StatusInternalServerError, err.Error())
    return true
  }
  return false
}


/*
func testDB() {
  dbinfo := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
    DB_HOST, DB_USER, DB_NAME)
  db, err := sql.Open("postgres", dbinfo)
  checkErr(err)
  defer db.Close()

  err = db.Ping()
  checkErr(err)

  fmt.Println("# Inserting values")

  var lastInsertId int
  err = db.QueryRow("INSERT INTO userinfo (username,departname,created) VALUES ($1,$2,$3) returning uid;", "astaxie", "研发部门", "2012-12-09").Scan(&lastInsertId)
  checkErr(err)
  fmt.Println("last inserted id =", lastInsertId)

  fmt.Println("# Updating")
  stmt, err := db.Prepare("update userinfo set username=$1 where uid=$2")
  checkErr(err)

  res, err := stmt.Exec("astaxieupdate", lastInsertId)
  checkErr(err)

  affect, err := res.RowsAffected()
  checkErr(err)

  fmt.Println(affect, "rows changed")

  fmt.Println("# Querying")
  rows, err := db.Query("SELECT * FROM userinfo")
  checkErr(err)

  for rows.Next() {
    var uid int
    var username string
    var department string
    var created time.Time
    err = rows.Scan(&uid, &username, &department, &created)
    checkErr(err)
    fmt.Println("uid | username | department | created ")
    fmt.Printf("%3v | %8v | %6v | %6v\n", uid, username, department, created)
  }

  fmt.Println("# Deleting")
  stmt, err = db.Prepare("delete from userinfo where uid=$1")
  checkErr(err)

  res, err = stmt.Exec(lastInsertId)
  checkErr(err)

  affect, err = res.RowsAffected()
  checkErr(err)

  fmt.Println(affect, "rows changed")
}
*/
func checkErr(err error) {
  if err != nil {
    panic(err)
  }
}
