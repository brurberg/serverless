package serve

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/brurberg/serverless/helper"
	"log"
	//"strconv"
	"io/ioutil"
	"os/exec"
	//"fmt"
	"encoding/json"
	//"encoding/gob"
	"net/http"
	"strings"
)

type progErr struct {
	StatusCode int    `json:"statuscode"`
	Error      string `json:"error"`
}

func Get(r *gin.Engine, name string) {
	r.GET("/"+name, func(c *gin.Context) {
		/*header, err := json.Marshal(c.Request.Header)
		if helper.CheckErr(c, err) {
			return
		}*/
		query, err := json.Marshal(c.Request.URL.Query())
		if helper.CheckErr(c, err) {
			return
		}
		defer c.Request.Body.Close()

		body, err := ioutil.ReadAll(c.Request.Body)
		if helper.CheckErr(c, err) {
			return
		}
		indx := strings.LastIndex(name, "/") + 1
		cmd := exec.Command("./"+name[:indx]+strings.ToLower(c.Request.Method)+name[indx:], c.Request.Header["Authorization"][0], string(query), string(body))
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			log.Println("Pipe", err)
			c.JSON(http.StatusInternalServerError, nil)
			return
		}
		defer stdout.Close()

		if err := cmd.Start(); err != nil {
			log.Println("Start", err)
			c.JSON(http.StatusInternalServerError, nil)
			return
		}

		ret, err := ioutil.ReadAll(stdout)

		if err := cmd.Wait(); err != nil {
			var retErr progErr
			json.Unmarshal(ret, &retErr)
			log.Println("Wait, ", err)
			c.JSON(retErr.StatusCode, retErr)
			return
		}

		c.Data(http.StatusOK, "application/json", ret)
	})
}

func Post(r *gin.Engine, name string) {
	r.POST("/"+name, func(c *gin.Context) {
		/*header, err := json.Marshal(c.Request.Header)
		if helper.CheckErr(c, err) {
			return
		}*/
		query, err := json.Marshal(c.Request.URL.Query())
		if helper.CheckErr(c, err) {
			return
		}
		defer c.Request.Body.Close()

		body, err := ioutil.ReadAll(c.Request.Body)
		if helper.CheckErr(c, err) {
			return
		}

		indx := strings.LastIndex(name, "/") + 1
		cmd := exec.Command("./"+name[:indx]+strings.ToLower(c.Request.Method)+name[indx:], c.Request.Header["Authorization"][0], string(query), string(body))
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			log.Println("Pipe", err)
			c.JSON(http.StatusInternalServerError, nil)
			return
		}
		defer stdout.Close()

		if err := cmd.Start(); err != nil {
			log.Println("Start", err)
			c.JSON(http.StatusInternalServerError, nil)
			return
		}

		ret, err := ioutil.ReadAll(stdout)

		if err := cmd.Wait(); err != nil {
			var retErr progErr
			json.Unmarshal(ret, &retErr)
			log.Println("Wait, ", err)
			c.JSON(retErr.StatusCode, retErr)
			return
		}

		c.Data(http.StatusOK, "application/json", ret)
	})
}
